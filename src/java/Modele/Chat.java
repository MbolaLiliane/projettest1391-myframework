/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import Annotation.MyAnnotation;
import Utilitaire.ViewModel;
import java.util.HashMap;

/**
 *
 * @author Mbola
 */
public class Chat {
    private String nom="Mimi";
    /**
     * @return the nom
     */
    
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @MyAnnotation(getUrl = "Nom")
    public ViewModel getAll(){
        ViewModel retour=new ViewModel();
        HashMap<String, Object> donnee=new HashMap<>();
        retour.setUrl("Resultat.jsp");
        String nom="Mimi";
        donnee.put("nom", nom);
        retour.setDonnee(donnee);
        return retour;
    }
}
